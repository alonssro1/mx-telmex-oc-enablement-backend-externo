FROM node:16

WORKDIR /usr/src/app

COPY package*.json .

# si es para produccion: npm install --omit dev

RUN npm install

COPY . .

EXPOSE 3000

CMD ["node", "./bin/www"]