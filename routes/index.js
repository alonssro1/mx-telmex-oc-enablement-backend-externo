var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/data', function(req, res, next) {
  res.status(200).json({ hello: "world" });
});

module.exports = router;
